<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 19-Jul-17
 * Time: 1:16 PM
 */

namespace App;


trait HasRole  // custom trait to expand User model
{

    // check if a user has a particular role or roles
    public function hasRole($role)
    {
        // check whether this user has particular role (one role)
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }

        // if there are several roles check whether array of all user's roles contains $role in the name column
        return !! $role->intersect($this->roles)->count();


        /* if (is_array($role)) {
             foreach ($role as $r) {
                 // call this method recursivelly
                 if ($this->hasRole()) {
                     return true;
                 }
             }
         }
         return false;*/
    }

    // assigns a role to a current user
    public function assignRole($role)
    {
        return $this->roles()->save(
            Role::whereName($role)->firstOrFail()
        );
    }
}