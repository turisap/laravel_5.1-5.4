<?php

namespace App\Http\Controllers;

use App\Post;
use Gate;
use Illuminate\Http\Request;

class PostsController extends Controller
{

    //authorization before viewing a post is implemented here
    public function show($id)
    {
        auth()->loginUsingId(1); //temporary

        $post = Post::findOrFail($id);

        // if a post doesn't belongs this user, don't show it ('show-post') is in AuthServiceProvider moved to template
        /*if (Gate::denies('update-post', $post)) {
            abort(403, 'Sorry, no sorry');
        }*/

        // here is the same logic as above
        //$this->authorize('update-post', $post);

        // another same way
        /*if(auth()->user()->can('update-post', $post)) {
            return 'You can update this';
        }*/

        /*if (Gate::denies('update', $post)) {
            abort(403, 'You have no access to this page');
        }*/

        return view('posts', compact('post'));
    }
}
