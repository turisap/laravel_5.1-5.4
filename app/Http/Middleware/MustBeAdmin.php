<?php

namespace App\Http\Middleware;

use Closure;


/*
 class of custom middleware
 */
class MustBeAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    // conditions for middleware
    public function handle($request, Closure $next)
    {
        //return user to the originally requested page
        if (auth()->check() && auth()->user()->admin == 1) {
            return $next($request);
        }

        // return to the homepage if user didn't go through the middleware
        return redirect('/');
    }
}
