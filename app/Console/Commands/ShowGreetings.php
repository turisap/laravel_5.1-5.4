<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ShowGreetings extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    // name of the console as well as parameter and option with a default values
    protected $signature = 'kirill:greet {name=tip} {--greeting=Hi}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // here we join argument from $signature to the greeting
        $this->info($this->option('greeting') . ', ' . $this->argument('name'));
    }
}
