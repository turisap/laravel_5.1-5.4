<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;        // use this channel for everyone (without rules for access the mesages) in broadcastOn()
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel; // private channels don't work just like that (need to write rules for authorization)
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class GotDrunk implements ShouldBroadcast // should broadcast implementation allows us to broadcas
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    public $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('drunkGuy');
    }

}
