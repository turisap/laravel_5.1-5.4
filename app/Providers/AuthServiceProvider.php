<?php

namespace App\Providers;

use App\Permission;
use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    // here we need to register our policies
    protected $policies = [
        'App\Post' => 'App\Policies\PostPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot(GateContract $gate) // pay attention to the respective use statement
    {
        $this->registerPolicies();

        // define a rule which checks whether this user was an author of  a particular post moved to template and Policy class
        /*$gate->define('update-post', function ($user, $post) {
            return $user->id  ==  $post->user_id;
        });*/

        // get all permissions and define policies for all of those permissions
        foreach ($this->getPermissions() as $permission) {

            $gate->define($permission->name, function ($user) use ($permission) {
                // test whether a user has a role with such a permission
                return $user->hasRole($permission->roles);
            });
        }
    }

    public function getPermissions()
    {
        // getting permissions with their roles in one query
        return Permission::with('roles')->get();
    }
}
