<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }


    // give a permission to a particular role (just saves it in the database)
    public function givePermissionTo($permission)
    {
        return $this->permissions()->save($permission);
    }

}
