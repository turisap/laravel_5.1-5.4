<?php

namespace App\Listeners;

use App\Events\GotDrunk;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CallHer
{
    /**
     * Create the event listener.
     *
     * @return void
     */


    public function __construct()
    {
    }

    /**
     * Handle the event.
     *
     * @param  GotDrunk  $event
     * @return void
     */
    public function handle(GotDrunk $event)
    {
        var_dump($event->name . ', you got drunk, let\'s call her right now!');
    }
}
