<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/test', 'TestController@test');

Route::get('/', function () {
    return view('welcome');
});

// here we injecting class Statistics to the view (moved to the view itself by @inject
Route::get('/resolve-data-to-view', function () {
    return view('resolve_data');
});



// route groups with a common prefix
Route::group(['prefix' => 'admin'], function () {
    Route::get('home', 'AdminController@index');
});


//events broadcasting
Route::get('/event', function () {
    event(new \App\Events\GotDrunk('Kirill'));
});

// posts
Route::resource('posts', 'PostsController');

// permissions
Route::get('/permissions', function () {

    auth()->loginUsingId(2); // id 2 and 3 have different permissions
    return view('welcome');
});