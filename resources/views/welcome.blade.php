<!doctype html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!--pusher library-->
        <script src="//code.jquery.com/jquery-2.1.3.min.js"></script>
        <script src="https://js.pusher.com/4.1/pusher.min.js"></script>

        <script>
            // Enable pusher logging - don't include this in production
            Pusher.logToConsole = true;


            var pusher = new Pusher('92508ac46fb1cce52102', {
                cluster: 'ap1',
                encrypted: true
            });


            var channel = pusher.subscribe('drunkGuy');
            channel.bind('App\\Events\\GotDrunk', function(data) {
                alert('nakanets to!')
            });

        </script>

    </head>
    <body>
        <h1>Welcome</h1>

    @can('edit_forum') <!--check if user has a permission to edit forum -->
        <a href="#">Edit forum</a>
    @endcan
    @can('manage_money') <!--check if user has a permission to manage money -->
        <a href="#">Manage money</a>
    @endcan

    </body>
</html>
