<!-- injecting data directly to the view, not using view composer of Service container-->
@inject('stats', '\App\Statistic')

<h1>Resolving data to the view</h1>
<h3>You have {{$stats->lessons()}} lessons</h3>
